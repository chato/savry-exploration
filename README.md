# Teaching materials based on SAVRY in Catalonia

The Python Notebook in this directory was created by [Carlos Castillo](http://chato.cl/research/) on January 2019 for educational purposes.

The dataset used here corresponds to a set of 850 juvenile offenders in Catalonia who were evaluated in 2010 using **SAVRY**, a structured risk assessment tool. The data on recidivism indicates if the same people committed a new offence in 2013-2015.

This is a sub-set of a dataset obtained by the *Centre d'Estudis Jurídics i Formació Especialitzada* of the Catalan government and released in 2017. The data was downloaded from [CEJFE / Generalitat de Catalunya](http://cejfe.gencat.cat/en/recerca/cataleg/crono/2017/reincidencia-jj/) and prepared by Marzieh Karimihaghighi and Carlos Castillo.

# Using this data

Do not use the data in this directory for research, as it is just a sub-set of attributes. Instead, use the [original dataset](http://cejfe.gencat.cat/en/recerca/cataleg/crono/2017/reincidencia-jj/) and cite the following publication:

```
@misc{cejfe_2017_savry,
 title={La reincid\`{e}ncia en la just\'{i}cia de menors},
 author={Marta Blanch and Manel Capdevila and Marta Ferrer and Berta Framis and \'{U}rsula Ruiz and Judit Mora and Ares Batlle and Berta L\'{o}pez},
 year={2017},
 howpublished={CEJFE},
 url={http://cejfe.gencat.cat/en/recerca/cataleg/crono/2017/reincidencia-jj/}
}
```

# Mapping of variables to English

A codebook is available at the above-cited URL. The following is the mapping from English variables in this dataset (a subset of the variables in CEJFE's study) to the original variable names in Catalan.

| Variable in English | Original variable in Catalan |
|---------------------|------------------------------|
|	gender	|	V1_sexe	|
|	nationality	|	V2_Foreigner	|
|	main_crime_age	|	V8_edat_fet	|
|	criminal_records	|	V12_nombre_ante_agrupat	|
|	main_crime_category	|	V15_fet_agrupat	|
|	main_crime_is_violent	|	V16_fet_violencia	|
|	risk01_previous_violence	|	V65_@1_violencia_previa	|
|	risk02_history_nonviolent_offences	|	V66_@2_historia_delictes_no_violents	|
|	risk03_early_violence_before_14	|	V67_@3_inici_precoç_violencia	|
|	risk04_past_intervention_fail	|	V68_@4_fracas_intervencions_anteriors	|
|	risk05_self_harm_suicide_attempts	|	V69_@5_intents_autolesio_suicidi_anteriors	|
|	risk06_violence_at_home	|	V70_@6_exposicio_violencia_llar	|
|	risk07_childhood_mistreatment	|	V71_@7_historia_mHighracte_infantil	|
|	risk08_criminal_parent_caregiver	|	V72_@8_delinquencia_pares	|
|	risk09_early_separation_parents	|	V73_@9_separacio_precoç_pares	|
|	risk10_poor_school_achievement	|	V74_@10_Low_rendiment_escola	|
|	risk11_delinquency_peer_group	|	V75_@11_delinquencia_grup_iguals	|
|	risk12_rejection_peer_group	|	V76_@12_rebuig_grup_iguals	|
|	risk13_poor_stress_coping	|	V77_@13_estrés_incapacitat_enfrontar_dificultats	|
|	risk14_poor_parental_skills_parents	|	V78_@14_escassa_habilitat_pares_educar	|
|	risk15_lack_of_personal_social_support	|	V79_@15_manca_suport_personal_social	|
|	risk16_marginalized_environment	|	V80_@16_entorn_marginal	|
|	risk17_negative_attitudes	|	V81_@17_actitud_negatives	|
|	risk18_risk_taker_impulsive	|	V82_@18_assumpcio_riscos_impulsivitat	|
|	risk19_drug_abuse	|	V83_@19_problemes_consum_toxics	|
|	risk20_anger_management_issues	|	V84_@20_problemes_maneig_enuig	|
|	risk21_low_empaty	|	V85_@21_Low_nivell_empatia_remordiment	|
|	risk22_attention_deficit	|	V86_@22_problemes_concentracio_hiperactivitat	|
|	risk23_poor_compliance_interventions	|	V87_@23_Lowa_colaboracio_intervencions	|
|	risk24_low_commitment_school	|	V88_@24_Low_compromis_escolar_laboral	|
|	protective01_prosocial_activities	|	V89_@P1_impicacio_prosocial	|
|	protective02_strong_social_support	|	V90_@P2_suport_social_fort	|
|	protective03_strong_link_prosocial_adult	|	V91_@P3_forta_vinculacio_adult_prosocial	|
|	protective04_positive_attitude_interventions	|	V92_@P4_actitud_positiva_intervencions_autoritat	|
|	protective05_high_commitment_school_or_work	|	V93_@P5_fort_compromis_escola_treball	|
|	protective06_perseverant_personality	|	V94_@P6_perseverança_tret_personalitat	|
|	average_01_antisocial	|	V95_FACT1mean_ANTISOCIAL	|
|	average_02_family	|	V96_FACT2mean_DINAMICAFAM	|
|	average_03_personality	|	V97_FACT3mean_PERSONALITAT	|
|	average_04_social_support	|	V98_FACT4mean_SUPORTSOCIAL	|
|	average_05_susceptibility_treatment	|	V99_FACT5mean_SUSCEPTIBILITAT	|
|	sum_historic_risk_items	|	V61_SAVRY_historics_total_score	|
|	sum_social_risk_items	|	V62_SAVRY_socials_total_score	|
|	sum_individual_risk_items	|	V63_SAVRY_individuals_total_score	|
|	sum_all_risk_items	|	V60_SAVRY_total_score	|
|	sum_all_protective_items	|	V64_SAVRY_proteccio_total_score	|
|	professional_risk_evaluation	|	V56_@R1_resum_risc_global_reverse	|
|	recidivism	|	V132_REINCIDENCIA_2013	|
|	recidivism_number_offences	|	V120_nombre_reincidencies_2013	|
|	recidividm_category_first_offence	|	V123_rein_fet_agrupat 	|
|	recidivism_violence_first_offence	|	V124_rein_fet_violencia_2013	|
|	recidivism_severity	|	V131_severitat_puntuacio_2013	|

# Technical description of the CEJFE study

**Territory**: Catalonia

**Contents**: Minors or young offenders who finished a measure or program in juvenile justice in 2010 (N=4,753). Samples from 9 previous studies have been retrieved and included as primary data, with a total of 14,681 juveniles who completed a program or measure between 2002 and 2010.

**Follow-up period**: From the end of the base case to 31 December 2013 (3.5 years on average). From the end of the base case to 31 December 2015 (5.5 years on average).

**Analysis**: Descriptive analyses, contingency tables, variance analysis, correlations analysis, reliability analysis, ROC curve analysis, odds ratio analysis and factor analysis. Study of descriptive and recidivism variables.

**Sources of the data**:

* SIJJ (Juvenile Justice Information System)
* SIMEPC (Community sanctions and measures Information System)
* SIPC (Penitentiary Information System of Catalonia)
* SAVRY (instrument for assessing the risk of recidivism in young people)

**Software used by CEJFE**: IBM SSO Statistics 22.0 Statistical Package
